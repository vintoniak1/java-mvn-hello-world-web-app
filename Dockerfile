FROM maven:3-openjdk-14-slim AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml install

FROM tomcat
COPY --from=build /home/app/target/mvn-hello-world.war /usr/local/tomcat/webapps
EXPOSE 8080
